from django.db import models

class ExchangeRate(models.Model):
    name = models.CharField(max_length=25)
    serie = models.CharField(max_length=7)
