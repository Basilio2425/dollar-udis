from django.contrib import admin
from exchange.models import ExchangeRate


@admin.register(ExchangeRate)
class AdminExchangeRate(admin.ModelAdmin):
    list_display = ('name', 'serie')