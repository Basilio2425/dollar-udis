from django.shortcuts import render
from django.views.generic import TemplateView
from exchange.models import ExchangeRate


class login(TemplateView):
    template_name = "login.html"


class home(TemplateView):
    def get(self, request):
        exchange = ExchangeRate.objects.all()
        context = {
            'exchange': exchange,
        }
        return render(request, 'index.html', context)
