from django.urls import path
from exchange import views
from exchange import api

urlpatterns = [
    path('', views.home.as_view(), name='home'),
    path('api/1.0/get_exchange', api.Exchange.as_view(), name="get_exchange")
]
