from rest_framework import serializers
import requests
from django.conf import settings


class ExchangeSerializer(serializers.Serializer):
    key = serializers.CharField()
    date_start = serializers.DateField(format="%Y-%m-%d")
    date_end = serializers.DateField(format="%Y-%m-%d")

    def get_exchange(self, validate_data):
        date_start = validate_data['date_start']
        date_end = validate_data['date_end']
        key = validate_data['key']
        url = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/'+str(key)+'/datos/'+str(date_start)+'/'+str(date_end)
        response = requests.get(
            url,
            headers={'Bmx-Token': settings.TOKEN_BANXICO},
        )
        elements = response.json()['bmx']['series'][0]['datos']
        list_data = []
        for data in elements:
            list_data.append(float(data['dato']))
        average = round(sum(list_data) / len(list_data), 6)
        maximum = max(list_data)
        minimum = min(list_data)
        r = {
            'graph': response.json(),
            'data': {
                'average': average,
                'maximum': maximum,
                'minimum': minimum
            }
        }
        return r
