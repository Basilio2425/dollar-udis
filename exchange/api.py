from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import ExchangeSerializer
#import requests
#from django.conf import settings


class Exchange(APIView):
    def post(self, request):
        serializer = ExchangeSerializer(data=request.data)
        if serializer.is_valid():
            r = serializer.get_exchange(validate_data=request.data)
            return Response(r)