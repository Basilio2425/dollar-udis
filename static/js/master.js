//Obtener los datos de la api a travéz de fetch
const getDataApi = async (info) => {
    const url = 'http://127.0.0.1:8000/api/1.0/get_exchange';
    const response = await fetch(url, {
      method: "POST",
      headers:{
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(info)
    })
    const json = await response.json(); 
    console.log(json)
    const { datos } = json.graph.bmx.series[0];
    const { average, maximum, minimum } = json.data; 
    const content = document.getElementById("extraInfo"); 

    let extraInfo = `<p>Promedio: ${average}</p>
                      <p>Máximo: ${maximum}</p>
                      <p>Mínimo: ${minimum}</p>`; 
    content.innerHTML = extraInfo; 
    drawGraphic(datos)
}
//Obtiene los datos al dar clien en el botón de submit 
function getDatos(event) {
    event.preventDefault();
    const { typeToSend, dateIni, dateEnd } = event.target.elements;
    const data = {
        key: typeToSend.value,
        date_start: dateIni.value,
        date_end: dateEnd.value
    }
    const validate = validateDate();
    if (data.dateEnd === "" || data.dateIni === "" || validate == false) return 0;
    console.log(data)
    getDataApi(data)
}
//Valida que la fecha sea válida (Menos a la fecha actual) 
function validateDate() {
  let date = new Date();
  const button = document.getElementById("buttonSubmit");
  const dateOne = new Date(document.getElementsByName("dateIni")[0].value);
  const dateTwo = new Date(document.getElementsByName("dateEnd")[0].value);
  let error = document.getElementById("error");
  let messageError = (document.getElementsByName("dateIni")[0].value == "" || document.getElementsByName("dateEnd")[0].value == "") ? "Todos los campos son obligatorios" : "No puedes seleccionar fechas posteriores al día actual.";

  error.innerHTML = (date > dateOne && date > dateTwo) ? "" : messageError;
  if (date > dateOne && date > dateTwo) {
    button.classList.remove("buttonDisabled");
    return true;
  }

  button.classList.add("buttonDisabled");
  return false;
}
//dibuja la grafica con la información que optiene de la api
function drawGraphic(data) {
  let dates =[]
  let datos = []
  data.map(elem =>  {
    let { fecha, dato } = elem;
    datos.push(dato)
    dates.push(fecha)
  })
  new Chart(document.getElementById("charts"), {
      type: 'line',
      data: {
        labels: dates,
        datasets: [{
            data: datos,
            label: document.getElementsByName("typeToSend")[0].value,
            borderColor: "#3e95cd",
            fill: false
          }
        ]
      },
      options: {
        title: {
          display: true,
          text: ''
        }
      }
    });
}