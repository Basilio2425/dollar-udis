def max_number_str(number):
    try:
        int(number)
        if len(number) <= 1000:
            serie = str(number)
            max = 0
            auxNum = 0
            i = 0
            for word in serie:
                auxNum = int(serie[i:i + 5])
                if auxNum > max:
                    max = auxNum
                i += 1
            return max
        else:
            message = "No se pueden ingresar números con longitud mayor a 1000"
            return message
    except ValueError:
        message = "No es una cadena de dígitos"
        return message


def main():
    print("Ingresar un nùmero que contenga menos de 1000 dígitos")
    number = input()
    resp = max_number_str(number)
    print(resp)

main()